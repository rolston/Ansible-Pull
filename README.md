# Ansible-Pull Deploy

## Overview
The following git project deploys Ansible-Pull configuration to Linux servers. The Ansible-Pull solution is a DevOps initiative to provide simplified security and compliance across multiple linux distros without complicated installations and dependencies. Below the installation process is broken down into configuration steps which can be manually performed to troubleshoot.



## Ansible-Pull Configurations ##
The following table defines the variables set in the Ansible-Pull configuration.

Configuration | Value | Description
------------- | ----- | -----------
cron scheudle | '*/45 * * * *' | Ansible-Pull is set to run every 15 minutes
cron user | runner | User to run ansible-pull as from cron
log file | /var/log/ansible-pull.log | File that ansible will use for logs
working directory | /var/lib/ansible/local | Directory to where repository will be cloned


### Service Account ###
The Ansible-Pull solution installs a local user account named runner which conducts all the configuration functionality. The runner account is granted sudo passwordless access allowing it to pull down the Ansible-Pull repository and execute the necessary Playbook

### Scheduling ###
The setup deploys Ansible-Pull to run a cron job under the runner account which executes every X minutes. The schedule is set in the crontab of the runner account.

### Logging ###
All Ansible-Pull operation are logged at /var/log/ansible-pull.log. Ownership of the log file belongs to the runner account which outputs the results of the cron job to the file location.

### Dependencies
The Ansibe-Pull system requires a limited set of dependecies that most system already have installed. The following required resources are installed through the Ansible-Pull deployment scripts:

1. [Git](https://gitlab.com/rolston/Ansible-Pull/blob/master/git/install-git.sh)
2. [Ansible](https://gitlab.com/rolston/Ansible-Pull/blob/master/ansible/install-ansiblev2.sh)
3. [runner account](https://gitlab.com/rolston/Ansible-Pull/blob/master/runner/create-runner.sh)

## Installation ##
Installation of Anisble-Pull and configuration of the application can be completed through the following scripts. It is best practive to incorporate the Ansible-Pull deployment through CloudFormation template for each Linux EC2 instance. 



To deploy the entire solution interactively from the console run the following to complete a full installation:

**wget**
```bash
wget -q https://gitlab.com/rolston/Ansible-Pull/master/pull/install-ansiblepull.sh -O-| sudo bash
```
**cURL**
```bash
curl -fsS https://gitlab.com/rolston/Ansible-Pull/master/pull/install-ansiblepull.sh | sudo bash
```

-----

### 1. Configuring the Runner Account

The runner account is the user account created to run the anisble-pull system. The following script will install the runner account

```bash
wget --no-check-certificate  -q https://gitlab.com/rolston/Ansible-Pull/master/runner/create-runner.sh -O-| sudo bash
```

To install the runner account using cURL run the follwing script

```bash
curl --insecure -fsS https://gitlab.com/rolston/Ansible-Pull/master/runner/create-runner.sh | sudo bash
```
-----
=====
### 2. Installing Ansible
Ansible can be installed through running the following script

```bash
wget --no-check-certificate -q https://gitlab.com/rolston/Ansible-Pull/master/ansible/install-ansible.sh -O-| sudo bash
```
or
```bash
curl --insecure -fsS https://gitlab.com/rolston/Ansible-Pull/master/ansible/install-ansible.sh | sudo bash
```
-----
=====
### 3. Installing Git
Git can be installed through running the following script

```bash
wget --no-check-certificate -q https://gitlab.com/rolston/Ansible-Pull/master/git/install-git.sh -O-| sudo bash
```
or
```bash
curl --insecure -fsS https://gitlab.com/rolston/Ansible-Pull/master/git/install-git.sh | sudo bash
```
-----
=====
### 4. Configuring Pull
To configure Ansible-Pull steps 1 and 2 will need to be completed. To save time, Step 3 can be run and regardless of whether you actually did steps 1 and 2, the script will call them and configure as needed.
```bash
wget --no-check-certificate -q https://gitlab.com/rolston/Ansible-Pull/master/pull/install-ansiblepull.sh -O-| sudo bash
```

```bash
curl --insecure -fsS -q https://gitlab.com/rolston/Ansible-Pull/master/pull/install-ansiblepull.sh | sudo bash
```