#!/bin/bash

# created by G Rolston
# TODO : epel install

# check if Ansible is already installed
if ! [ -x "$(command -v ansible)" ]; then
  # Use the OS package manager to install for Red Hat Enterprise Linux (TM), CentOS, Fedora, AWS Linux and Oracle
  if [ -f /etc/centos-release ] || [ -f /etc/redhat-release ] || [ -f /etc/oracle-release ] || [ -f /etc/system-release ] || grep -q 'Amazon Linux' /etc/system-release; then
    yum -y install ca-certificates nss
    if  grep -q 'Red Hat Enterprise Linux Server release 7.' /etc/system-release ; then
      wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-9.noarch.rpm
      rpm -ivh epel-release-7-9.noarch.rpm
    elif  grep -q 'Red Hat Enterprise Linux Server release 6.' /etc/system-release ; then 
      wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
      rpm -ivh epel-release-6-8.noarch.rpm
    elif  grep -q 'Amazon Linux' /etc/system-release ; then
      wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
      rpm -ivh epel-release-6-8.noarch.rpm
    else
      yum install epel-release -y
    fi

    yum -y install python-pip
    yum install ansible -y

    # this checks to make sure pip installed and if not it will use easy install
    # If python-pip install failed and setuptools exists, try that
    if [ -x "$(command -v pip)" -a -z "$(command -v easy_install)" ]; then
      yum -y install python-setuptools
      easy_install pip
    elif [ -x "$(command -v pip)" -a -n "$(command -v easy_install)" ]; then
      easy_install pip
    fi
    
  # Use the OS package manager to install for Debian, or Ubuntu
  elif [ -f /etc/debian_version ] || [ grep -qi ubuntu /etc/lsb-release ] || grep -qi ubuntu /etc/os-release; then
    apt-get install software-properties-common -y
    apt-add-repository ppa:ansible/ansible
    apt-get update -y
    apt-get install ansible -y
  # Try to use pip if it is installed to install ansible
  elif [ -x "$(command -v pip)" ]; then
    pip install ansible
  else
      echo 'WARN: Could not detect distro or distro unsupported'
      echo 'WARN: Pip not detected'
  fi
else
  echo 'INFO: Ansible is already installed'
fi
if ! [ -d /etc/ansible/ ]; then
  mkdir /etc/ansible/
fi


# check to see if ansible was installed
if ! [ -x "$(command -v ansible)" ];
then 
  # if not installed correctly, try using pip if available
  if [ -x "$(command -v pip)" ]; then
    pip install ansible
  else
      echo 'WARN: Could not detect distro or distro unsupported'
      echo 'WARN: Pip not detected'
  fi
fi   
# setup localhost in the inventory file
echo -e '[local]\nlocalhost\n' > /etc/ansible/hosts