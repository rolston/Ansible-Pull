#!/bin/bash

cd /root/ 
# create the runner account if it does not exist
curl --insecure -fsS -q https://github.usgs.chs.ead/raw/USGS-CHS/AnsiblePull-Deploy/master/runner/create-runner.sh -o /root/create-runner.sh
chmod u+x /root/create-runner.sh
./create-runner.sh
# install ansible if not already installed
curl --insecure -fsS https://github.usgs.chs.ead/raw/USGS-CHS/AnsiblePull-Deploy/master/ansible/install-ansiblev2.sh -o /root/install-ansiblev2.sh
chmod u+x /root/install-ansiblev2.sh
./install-ansiblev2.sh
# install git if not already installed
curl --insecure -fsS https://github.usgs.chs.ead/raw/USGS-CHS/AnsiblePull-Deploy/master/git/install-git.sh -o /root/install-git.sh
chmod u+x /root/install-git.sh
./install-git.sh

sleep 15

# setup supporting directories
if ! [ -d /var/lib/ansible/local ];
then
  mkdir /var/lib/ansible/local -p
fi
chown -hR runner:runner /var/lib/ansible/local
touch /var/log/ansible-pull.log
chown runner:runner /var/log/ansible-pull.log

# configure runner git to ignore ssl errors
echo '[user]' > /home/runner/.gitconfig
echo 'name = runner' >> /home/runner/.gitconfig
echo 'email = runner@usgs.gov' >> /home/runner/.gitconfig
echo '[http]' >> /home/runner/.gitconfig
echo 'sslVerify = false' >> /home/runner/.gitconfig
chown runner:runner /home/runner/.gitconfig



# Build the cron job to complete Ansible-Pull setup
if grep -q 'Amazon Linux' /etc/system-release ; then
  # AWS Linux
  echo "*/15 * * * * /usr/local/bin/ansible-pull -U https://github.usgs.chs.ead/USGS-CHS/Ansible-Pull.git -d /var/lib/ansible/local >> /var/log/ansible-pull.log  2>&1" > /root/AnsiblePullCron
elif [ -f /etc/debian_version ] || [ grep -qi ubuntu /etc/lsb-release ] || grep -qi ubuntu /etc/os-release; then
  # ubuntu 16.04 /usr/bin/ansible-pull -verified
  echo "*/15 * * * * /usr/local/bin/ansible-pull -U https://github.usgs.chs.ead/USGS-CHS/Ansible-Pull.git -d /var/lib/ansible/local >> /var/log/ansible-pull.log  2>&1" > /root/AnsiblePullCron
else  
  pullcmd=$(command -v ansible-pull)
  if [ -x $pullcmd ]; then
    echo "*/15 * * * * $pullcmd -U https://github.usgs.chs.ead/USGS-CHS/Ansible-Pull.git -d /var/lib/ansible/local >> /var/log/ansible-pull.log  2>&1" > /root/AnsiblePullCron
  else
    echo "*/15 * * * * /usr/local/bin/ansible-pull -U https://github.usgs.chs.ead/USGS-CHS/Ansible-Pull.git -d /var/lib/ansible/local >> /var/log/ansible-pull.log  2>&1" > /root/AnsiblePullCron
  fi
fi
crontab -u runner /root/AnsiblePullCron