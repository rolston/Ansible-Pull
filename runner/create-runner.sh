#!/bin/bash

# create the runner account if it does not exist
getent passwd runner > /dev/null
if [ $? -ne 0 ]
then
    echo "Creating runner account ..."
	useradd -m -s /bin/bash runner
else
    echo "runner account exists - skipping creation"
fi

# add account to the sudoers file if it does not exist
cat /etc/sudoers|grep ^runner > /dev/null
if [ $? -ne 0 ]
then
    echo "Adding runner account to the sudoers file ..."
	echo "runner        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
	echo "Defaults:runner !requiretty" >> /etc/sudoers
	echo "Defaults:root !requiretty" >> /etc/sudoers
else
    echo "runner account already in sudoers file - skipping"
fi

# configure the ssh pub key for login if it does not exist
if [ ! -s /home/runner/.ssh/authorized_keys ]
then
    if [ ! -d /home/runner/.ssh ]
	then 
		echo "Creating .ssh directory ..."
		mkdir /home/runner/.ssh
	else
	 	echo ".ssh directory already exists - skipping"
	fi
    
    # create the authorized key file for access
	# the public key configuration will allow ansible
	# to run in push mode if necessary.
    echo "Adding authorized_keys file ..."
	curl --insecure -o /home/runner/.ssh/authorized_keys https://github.usgs.chs.ead/raw/USGS-CHS/AnsiblePull-Deploy/master/runner/keys/public/id_rsa.pub
	chown runner:runner -R /home/runner/.ssh
	chmod 600 /home/runner/.ssh/authorized_keys
else
	echo "authorized_key file already exists - skipping"
fi

# create the deployment key used by Anisbile-Pull to download repo.
# the public key for the deployment is saved on github and has read
# access only.The key-pair has no other access.
if [ ! -s /home/runner/.ssh/id_rsa ]
then
    echo "Adding ansible repository deploy key"
    curl --insecure -o /home/runner/.ssh/id_rsa https://github.usgs.chs.ead/raw/USGS-CHS/AnsiblePull-Deploy/master/runner/keys/git/id_rsa
    chmod 600 /home/runner/.ssh/id_rsa
fi