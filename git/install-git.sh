#!/bin/bash

if [ -f /etc/centos-release ] || [ -f /etc/redhat-release ] || [ -f /etc/oracle-release ] || [ -f /etc/system-release ] || grep -q 'Amazon Linux' /etc/system-release; 
then
  # installation for RHEL, CentOS, Fedora, and AWS Linux
  yum install git -y
elif [ -f /etc/debian_version ] || [ grep -qi ubuntu /etc/lsb-release ] || grep -qi ubuntu /etc/os-release; then
  # someone screwed up and could be using ubuntu
  echo 'WARN: You are using something related to Ubuntu. Git will be installed; however, overall enterprise experience will be terrible.'
  apt-get install git -y
else
    echo 'WARN: Could not detect distro or distro unsupported'
fi